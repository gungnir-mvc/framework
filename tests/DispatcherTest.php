<?php
namespace Gungnir\Framework\Tests;

use \Gungnir\Framework\Dispatcher;
use \Gungnir\Core\Container;

class DispatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testItCanBeInstantiated()
    {
        $container = new Container;
        $dispatcher = new Dispatcher($container);
        $result = $dispatcher->run();
        $this->assertInstanceOf(Dispatcher::class, $dispatcher);
    }
}
